var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');

var index = require('./routes/index');
var users = require('./routes/users');
var olqv = require('./routes/olqv');
var connectArtemix = require('./routes/connectArtemix')
var fitsBrowser = require('./routes/fitsBrowser');
var monitor = require('./routes/monitor');
var purge = require('./routes/purge');
const dirs = require('./dirs');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'png')));
app.use('"'+dirs.SAMP_DIR+'"', express.static(path.join(__dirname, '"'+dirs.SAMP_DIR+'"')));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//app.use("/", index);

app.use('/browse', fitsBrowser);
app.use('/browse/getEntries', fitsBrowser);
app.use('/connectArtemix', connectArtemix);
app.use('/visit', olqv);
app.use('/users', users);
app.use('/', fitsBrowser);
app.use('/monitor', monitor);
app.use('/purge', purge);
app.use('/', fitsBrowser);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  //res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.locals.error =  err;

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// SAMP_DIR cleaning of temporary FITS files scheduled
let regex1 = /spectrum\.[a-f0-9\-]+\.fits$/;
let regex2 = /averageSpectrum\.[a-f0-9\-]+\.fits$/;

function clearSAMPFITSFiles() {
  console.log("About to clean spectrum and averageSpectrum FITS files found in " + dirs.SAMP_DIR);
  fs.readdirSync(dirs.SAMP_DIR).filter(f => regex1.test(f)).map(f => fs.unlinkSync(dirs.SAMP_DIR+"/"+f));
  fs.readdirSync(dirs.SAMP_DIR).filter(f => regex2.test(f)).map(f => fs.unlinkSync(dirs.SAMP_DIR+"/"+f));
}

setInterval(clearSAMPFITSFiles, 60000);

console.log("Ready to serve!");
module.exports = app;
