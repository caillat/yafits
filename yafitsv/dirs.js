// Directories definitions.
const PNG_ROOT_DIR = '/home/partemix/dataroot/PNG';
const OBJ_ROOT_DIR = '/home/partemix/dataroot/OBJ';
const SAMP_DIR = '/home/partemix/dataroot/SAMP';

exports.PNG_ROOT_DIR = PNG_ROOT_DIR;
exports.OBJ_ROOT_DIR = OBJ_ROOT_DIR;
exports.SAMP_DIR = SAMP_DIR;
