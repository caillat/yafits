class Reset {
    constructor(viewer, infosBlock) {
        this.button = document.createElement("button");
        this.button.setAttribute("type", "button");
        this.button.setAttribute("class", "btn btn-primary btn-sm");
        this.button.setAttribute("data-tooltip", "tooltip");
        this.button.setAttribute("title", "Reset zoom and position");
        this.infosBlock = infosBlock;
        let x = document.createElement("span");
        x.setAttribute("class", "fas fa-home");
        this.button.appendChild(x);


        $(this.button).on("click", function (event) { viewer.reset() });
    }

    getButton() {
        return this.button;
    }
}