/*
** A class to build an HTML table containing the header of a FITS table presented in certain way
** and control its visibility.
**
** @itsDIV the div which will contain the FITS header table.
** @itsFITSHeaders a dictionary Key:Value containing the FITS header.
**
*/
function ENTER(msg) {
  console.log(msg + " : entering");
};

function EXIT(msg) {
  console.log(msg + " : exiting");
};

/*
** The constructor.
** Expects two parameters :
**
** @itsDIV a reference to a div which will contain the tabular presentation of the FITS header.
** @itsFITSHeader a reference to the FITS Header to be displayed.
*/
function FITSHeaderTable( itsDIV, itsFITSHeader ) {
  ENTER("function FITSHeaderTable( itsDIV, itsFITSHeader )");

  itsDIV.id="FITSHeaderWrapper";
  itsDIV.class="overlay";
  itsDIV.style="overflow:scroll";

  let _innerHTML = "<a href=\"javascript:void(0)\" class=\"closebtn\" id=\"hideFITSHEADER\" >&times;</a>";
  _innerHTML += "<div class=\"overlay-content\">";
  _innerHTML += "<div id=\"FITSHeaderParent\">";
  _innerHTML += "<div id=\"FITSHeaderTable\" class=\"scrollWrapper\">";
  _innerHTML += "<table></table>";
  _innerHTML += "</div";
  _innerHTML += "</div";
  _innerHTML += "</div";

  itsDIV.innerHTML = _innerHTML;

  this.show = function() {
    _show();
  };

  var _show = function() {
    itsDIV.style.height = "100%";
  };

  this.hide = function() {
    _hide();
  };

  var _hide = function() {
    itsDIV.style.height = "0%";
  }

  $("#hideFITSHEADER").click(function(e) {_hide();});

  let _historyKeywords_ = ["DATE", "ORIGIN", "BLOCKED"];
  let _describingObservationKeywords_ = ["DATE-OBS","TELESCOP", "INSTRUME", "OBSERVER", "EQUINOX","EPOCH"];
  let _mandatoryKeywords_ = ["SIMPLE", "BITPIX", "NAXIS", "NAXIS1", "NAXIS2", "NAXIS3", "NAXIS4"];
  let _beamSizeKeywords_ = ["BMIN", "BMAJ", "BPA"];
  let _restFrqKeywords_ = ["RESTFRQ"];
  let _arrayKeywords_ = ["BSCALE", "BZERO", "BUNIT", "BLANK", "CTYPE1", "CTYPE2", "CTYPE3", "CTYPE4", "CRPIX1", "CRPIX2", "CRPIX3", "CRPIX4", "CRVAL1", "CRVAL2", "CRVAL3", "CRVAL4", "CDELT1", "CDELT2", "CDELT3", "CDELT4", "CROTA1", "CROTA2", "CROTA3", "CROTA3", "CUNIT1", "CUNIT2", "CUNIT3", "CUNIT4", "DATAMIN", "DATAMAX"];
  let _obsGeoKeywords_ = ["OBSGEO-X", "OBSGEO-Y", "OBSGEO-Z", "OBSRA", "OBSDEC"];

  let _keywordsByRegex = function(header, re) {
    var kw, result = [];
    for (kw in header) {
      if (kw.match(re) && header.hasOwnProperty(kw)) {
        result[result.length] = kw;
      }
    }
    return result.sort();
  }

  let _extractKeywords = function(keywords, header) {
    var result=[];
    for (var mk in keywords) {
      if (keywords[mk] in header) {
        result[keywords[mk]] = header[keywords[mk]];
        delete header[keywords[mk]];
      }
    }
    return result.sort();
  }

  let _populate = function (h) {
    var t = $('#FITSHeaderTable > table');
    t.empty();

    t.append('<tr><td><span>OBJECT: ' + h["OBJECT"] + '</span></td></tr>');
    t.append('<tr><td>--------</td><td>----------</td></tr>');

    /* History keywords */
    var mks = _extractKeywords(_historyKeywords_, h);
    for (var mk in mks) {
      t.append('<tr><td><span>' + mk + '</span></td><td><span>' + mks[mk] + '</span></td></tr>');
    }
    t.append('<tr><td>--------</td><td>----------</td></tr>');

    /* describing observation keywords */
    var mks = _extractKeywords(_describingObservationKeywords_, h);
    for (var mk in mks) {
      t.append('<tr><td><span>' + mk + '</span></td><td><span>' + mks[mk] + '</span></td></tr>');
    }
    t.append('<tr><td>--------</td><td>----------</td></tr>');

    /* The rest frequency */
    var mks = _extractKeywords(_restFrqKeywords_, h);
    for (var mk in mks) {
      t.append('<tr><td><span>' + mk + '</span></td><td><span>' + mks[mk] + '</span></td></tr>');
    }
    t.append('<tr><td>--------</td><td>----------</td></tr>');

    /* The mandatory keywords */
    var mks = _extractKeywords(_mandatoryKeywords_, h);
    for (var mk in mks) {
      t.append('<tr><td><span>' + mk + '</span></td><td><span>' + mks[mk] + '</span></td></tr>');
    }
    t.append('<tr><td>--------</td><td>----------</td></tr>');

    /* The beam size keywords */
    var mks = _extractKeywords(_beamSizeKeywords_, h);
    for (var mk in mks) {
      t.append('<tr><td><span>' + mk + '</span></td><td><span>' + mks[mk] + '</span></td></tr>');
    }
    t.append('<tr><td>--------</td><td>----------</td></tr>');

    /* The array keywords */
    var mks = _extractKeywords(_arrayKeywords_, h);
    for (var mk in mks) {
      t.append('<tr><td><span>' + mk + '</span></td><td><span>' + mks[mk] + '</span></td></tr>');
    }
    t.append('<tr><td>--------</td><td>----------</td></tr>');

    /* The geographic location of the observation (m) */
    var mks = _extractKeywords(_obsGeoKeywords_, h);
    for (var mk in mks) {
      t.append('<tr><td><span>' + mk + '</span></td><td><span>' + mks[mk] + '</span></td></tr>');
    }
    t.append('<tr><td>--------</td><td>----------</td></tr>');

    /* The PC keywords */
    var PCKeywords = _keywordsByRegex(h, /PC\d\d/);
    var mks = _extractKeywords(PCKeywords, h);
    for (var mk in mks) {
      t.append('<tr><td><span>' + mk + '</span></td><td><span>' + mks[mk] + '</span></td></tr>');
    }
    t.append('<tr><td>--------</td><td>----------</td></tr>');

    /* The rest */
    kws=[];
    for (var kw in h) {
      kws[kws.length] = kw;
    }

    /* is listed by alphabetical order */
    kws = kws.sort();
    for (i = 0; i < kws.length; i++)  {
      t.append('<tr><td><span>' + kws[i] + '</span></td><td><span>' + h[kws[i]] + '</span></td></tr>');
    }
  }

  var hclone = $.extend({}, itsFITSHeader);
  _populate(hclone);

  EXIT("function FITSHeaderTable( itsDIV, h)");
};
