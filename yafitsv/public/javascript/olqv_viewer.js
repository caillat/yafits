class Viewer {
    static enter(what) {
        console.group(this.name + "." + what);
    }

    static exit() {
        console.groupEnd();
    }

    constructor(relFITSFilePath, width, height, divId, canvasId, coordinatesFormatter, is3D) {
        Viewer.enter(this.constructor.name);
        this.relFITSFilePath = relFITSFilePath;
        this.with = width;
        this.height = height;
        this.divId = divId;
        this.canvas = document.getElementById(canvasId);
        this.is3D = is3D;

        this.coordinatesFormatter = coordinatesFormatter;

        this.layer = null;
        this.graticule = null;

        this.canvas.width = width;
        this.canvas.height = height;
        this.extent = [0, 0, width - 1, height - 1];

        this.projection = new ol.proj.Projection({
            code: 'local_image',
            units: 'pixels',
            extent: this.extent,
            worldExtent: [...this.extent]
        });

        this.controls = [
            new ol.control.MousePosition({
                undefinedHTML: '',
                coordinateFormat: this.coordinatesFormatter.format.bind(this.coordinatesFormatter)
            }),
            new ol.control.FullScreen({ source: 'fullscreen' })
        ];

        this.map = new ol.Map({
            target: this.divId,
            view: new ol.View({
                projection: this.projection,
                center: ol.extent.getCenter(this.extent),
                resolution: 1
            })
        });

        this.graticule = new ol.Graticule({
            showLabels: true,
            strokeStyle: new ol.style.Stroke({
                color: 'rgba(0,0,0,0.9)',
                width: 1,
                lineDash: [0.5, 4]
            }),
            //targetSize: 75,
            lonLabelFormatter: this.coordinatesFormatter.getRaLabelFormatter().format.bind(this.coordinatesFormatter.getRaLabelFormatter()),
            lonLabelStyle: new ol.style.Text({
                font: '12px Calibri,sans-serif',
                textBaseline: 'bottom',
                fill: new ol.style.Fill({ color: 'rgba(0,0,0,1)' }),
                stroke: new ol.style.Stroke({ color: 'rgba(255,255,255,1)', width: 3 })
            }),
            latLabelFormatter: this.coordinatesFormatter.getDecLabelFormatter().format.bind(this.coordinatesFormatter.getDecLabelFormatter())
        });

        this.graticule.setMap(this.map);

        /*
        ** One should be able to select any shape with a click.
        */
        this.select = new ol.interaction.Select({ condition: ol.events.click });
        this.map.addInteraction(this.select);

        this.select.on('select', event => {
            let selected_features = event.target.getFeatures();
            let deselected_features = event.deselected;

            if (selected_features.getLength() == 1) {
                let selectedFeature = selected_features.item(0);
                let featureType = selectedFeature.getGeometry().getType();
                if (featureType in this.selectResponses) {
                    this.selectResponses[featureType](selectedFeature, event);
                }
                else {
                    console.log(`Nothing to do for a ${featureType}`);
                }
            }
        });

        this.selectResponses = {};

        this.sliceRange = null;
        this.dataSteps = null;
        this.statistics = null;

        document.getElementById(divId).addEventListener("mouseenter", () => {
            this.controls.forEach((control) => {
                this.map.addControl(control);
            })
        });

        document.getElementById(divId).addEventListener("mouseexit", () => {
            this.controls.forEach((control) => {
                this.map.removeControl(control);
            })
        });
        Viewer.exit();
    }

    getDivId() {
        return this.divId;
    }

    getRelFITSFilePath() {
        return this.relFITSFilePath;
    }

    getMap() {
        return this.map;
    }

    getStatistics() {
        return this.statistics;
    }

    getSliceRange() {
        return this.sliceRange;
    }

    reset() {
        this.map.getView().setCenter(ol.extent.getCenter(this.extent));
        this.map.getView().setResolution(1);
    }

    imageLoadFunction(image, src) {
        Viewer.enter(this.imageLoadFunction.name);
        document.getElementById('loading').style.display = 'block';
        image.getImage().addEventListener('load', () => {
            document.getElementById('loading').style.display = 'none';
            this.canvas.getContext('2d').drawImage(image.getImage(), 0, 0);
        })
        image.getImage().src = src;
        image.getImage().crossOrigin = "Anonymous";
        Viewer.exit();
    }

    display(imageURL, sliceRange, dataSteps, statistics) {
        Viewer.enter(this.display.name);
        this.sliceRange = sliceRange;
        this.coordinatesFormatter.setDataSteps(dataSteps);
        this.statistics = statistics;
        if (this.layer) {
            this.map.removeLayer(this.layer);
        }
        this.layer = new ol.layer.Image({
            source: new ol.source.ImageStatic({
                url: imageURL,
                projection: this.projection,
                imageExtent: this.extent,
                imageLoadFunction: this.imageLoadFunction.bind(this),
            })
        });
        this.map.addLayer(this.layer);
        Viewer.exit();
    }

    getSelectResponses() {
        return this.selectResponses;
    }

    isThreeD(){
        return this.is3D;
    }

    couple(viewer) {
        var updateView = function (event, viewRef) {
            let newValue = event.target.get(event.key);
            viewRef.set(event.key, newValue);
        };
        
        this.map.getView().on('change:resolution', function (event) {
            updateView(event, viewer.getMap().getView());
        });

        this.map.getView().on('change:center', function (event) {
            updateView(event, viewer.getMap().getView());
        });
    }
}
