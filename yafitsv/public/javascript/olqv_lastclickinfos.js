class LastClickInfos {
    static enter(what) {
        console.group(this.name + "." + what);
    }

    static exit() {
        console.groupEnd();
    }

    constructor(viewer) {
        LastClickInfos.enter(this.constructor.name);
        this.map = viewer.getMap();
        this.popupId = `popup_${viewer.getDivId()}_${Date.now()}`
        this.popupCloserId = `${this.popupId}-closer`;
        this.popupContentId = `${this.popupId}-content`;
        this.html = `
<div class="hidden">
    <div id="${this.popupId}" class="ol-popup">
        <a href="#" id="${this.popupCloserId}" class="ol-popup-closer"></a>
        <div id="${this.popupContentId}"></div>
    </div>
</div>`.trim();
        this.popup = createAndAppendFromHTML(this.html, $(this.map.getTargetElement()));
        this.popupContent = $(`#${this.popupContentId}`);
        this.popupCloser = $(`#${this.popupCloserId}`);
        this.overlay = new ol.Overlay({
            element: this.popup,
            autoPan: true,
            autoPanAnimation: { duration: 250 }
        });

        this.popupCloser.click(() => { this.overlay.setPosition(undefined); this.popupCloser.blur() });
        this.map.addOverlay(this.overlay);

        this.lastChanIndex = null;
        this.lastCoordinate = null;
        this.lastRADEC = null;
        this.lastFluxDensity = null;

        LastClickInfos.exit();
    }

    apply() {
        LastClickInfos.enter(this.apply.name);
        if (this.lastCoordinate) {
            this.popupContent.innerHTML = `Chan#${this.lastCoordinate}<br>
            , x = ${this.lastCoordinate[0].toFixed(0)}, y = ${lastCoordinate[1].toFixed(0)}<br>
            RA= ${this.lastRADEC['RA']}<br>
            DEC=${this.lastRADEC['DEC']}<br>
            Value=${Number(this.lastFluxDensity).toExponential(4)}
            `;
            this.overlay.setPosition(this.lastCoordinate);
        }
        LastClickInfos.exit();
    }

    setPositionAndFluxDensity(data) {
        LastClickInfos.enter(this.setPositionAndFluxDensity.name);
        this.lastChanIndex = data["chanIndex"]
        this.lastCoordinate = data["coordinate"];
        this.lastRADEC = data["RADEC"];
        this.lastFluxDensity = data["fluxDensity"];

        this.apply();
        LastClickInfos.exit();
    }
    setFluxDensity(fluxDensity, sliceIndex) {
        LastClickInfos.enter(this.setFluxDensity.name);
        this.lastFluxDensity = fluxDensity;
        this.lastChanIndex = sliceIndex;
        LastClickInfos.exit();
        this.apply();
    };
}