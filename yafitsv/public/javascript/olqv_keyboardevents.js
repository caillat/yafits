class KeyCodeProcessor {

    static enter(what) {
        console.group(this.name + "." + what);
    }

    static exit() {
        console.groupEnd();
    }

    constructor(viewer) {
        KeyCodeProcessor.enter(this.constructor.name);
        this.viewer = viewer;
        KeyCodeProcessor.exit()
    }

    open() {
        KeyCodeProcessor.enter(this.open.name);
        document.addEventListener('keydown', this.process.bind(this), false);
        KeyCodeProcessor.exit();
    }

    process(event) {
        KeyCodeProcessor.enter(this.process.name);
        let ctrlKey = event.ctrlKey;
        let keyCode = event.keyCode;
        console.log(event);
        console.log(ctrlKey);
        console.log(keyCode);
        KeyCodeProcessor.exit();
    }
}
