/*
** A set of classes and function definitions utilized by the
** differents flavours of OLQV viewers.
**
** Author : M. Caillat
** Date : 06th December 2018
*/

/*
** A class to convert a right ascension expressed in decimal degree into an integer value expressing a pixel index.
*/
var RADDtoPixelConverter = function(radd0, radd1, rapix0, rapix1) {
    console.log("var RADDtoPixelConverter = function(radd0, radd1, rapix0, rapix1) { : entering");
    var _radd0 = radd0;
    var _rapix0 = rapix0;
    var _slope = (rapix1 - rapix0) / (radd1 - radd0);
    console.log("_radd0 = " + _radd0 + ", _rapix0 = " + _rapix0);

    this.convert = function(radd) {
      return _rapix0 + (radd - _radd0) * _slope;
    }
    console.log("var RADDtoPixelConverter = function(radd0, radd1, rapix0, rapix1) { : exiting");
};

/*
** A class to convert a declination expressed in decimal degree into an integer value expressing a pixel index.
*/
var DECDDtoPixelConverter = function(decdd0, decdd1, decpix0, decpix1) {
    var _decdd0 = decdd0;
    var _decpix0 = decpix0;
    var _slope = (decpix1 - decpix0) / (decdd1 - decdd0);

    this.convert = function(decdd) {
      return _decpix0 + (decdd - _decdd0) * _slope;
    }
};

/*
** Converts a decimal number expected to represent an angle in degree
** into a string expressing a right ascension ( H:M:S)
*/
var DecDeg2HMS = function(deg,sep=':'){
  //if(any(deg< 0 | deg>360)){stop('All deg values should be 0<=d<=360')}
  //if (deg < 0)
  //deg[deg < 0] = deg[deg < 0] + 360
  HRS = Math.floor(deg/15);
  MIN = Math.floor((deg/15 - HRS) * 60);
  SEC = (deg/15 - HRS - MIN/60) * 3600;
  SEC = Math.floor(SEC*1000) / 1000.;
  return HRS+sep+MIN+sep+SEC.toFixed(3);
};

/*
** Converts a decimal number expected to represent an angle in degree
** into a string expressing a declination ( D:M:S)
*/
var DecDeg2DMS = function(deg,sep=':'){
  var sign = deg < 0 ? '-':'+';
  deg = Math.abs(deg);
  var DEG = Math.floor(deg);
  var MIN = Math.floor((deg - DEG) * 60);
  var SEC = (deg - DEG - MIN/60) * 3600;
  SEC = Math.floor(SEC*1000.) / 1000.;
  if (SEC < 0.) SEC = 0.; if (SEC > 60) SEC = 60.;

  return(sign + DEG + ':' + MIN + ':' + SEC.toFixed(3));
};

/*
** A class to convert pixels into a string expressing a right ascension.
**
** The constructor establishes the transformation pixels -> HMS
** with the given parameter ra0pix, ra1pix ( interval in pixels )
** and ra0, ra1 ( the same interval in decimal degrees)
*/
 class RaLabelFormatter  {
  static enter(what) {
     console.group(this.name + "." + what);
  }

  static exit() {
     console.groupEnd();
  }

  constructor(ra0pix, ra1pix, ra0, ra1) {
    RaLabelFormatter.enter(this.constructor.name);
    this.ra0pix = ra0pix;
    this.ra1pix = ra1pix;
    this.ra0 = ra0;
    this.ra1 = ra1;
    this.slope = ((this.ra1 - this.ra0) / (this.ra1pix - this.ra0pix));
    RaLabelFormatter.exit();
  }

  /*
  ** Returns the string representation of a RA in HMS given its input value in pixels.
  */
  format(rapix) {
    //RaLabelFormatter.enter(this.format.name);
    var res = this.ra0 + (rapix - this.ra0pix) * this.slope;
    //RaLabelFormatter.exit();
    return DecDeg2HMS(res);
  }
};

/*
** A class to convert pixels into a string expressing a declination.
**
** The constructor establishes the transformation pixels -> DMS
** with the given parameter dec0pix, dec1pix ( interval in pixels )
** and dec0, dec1 ( the same interval in decimal degrees)
*/
class DecLabelFormatter {
  static enter(what) {
    console.group(this.name + "." + what);
  }

  static exit() {
    console.groupEnd();
  }

  constructor(dec0pix, dec1pix, dec0, dec1) {
    DecLabelFormatter.enter(this.constructor.name);
    this.dec0pix = dec0pix;
    this.dec1pix = dec1pix;
    this.dec0 = dec0;
    this.dec1 = dec1;
    this.slope = ((this.dec1 - this.dec0) / (this.dec1pix - this.dec0pix));
    DecLabelFormatter.exit();
  }

  format(decpix) {
    //DecLabelFormatter.enter(this.format.name);
    var res = this.dec0 + (decpix - this.dec0pix) * this.slope;
    //DecLabelFormatter.exit();
    return DecDeg2DMS(res);
  }
};

/*
** A function which returns the units of a spectrum resulting
** from the summation of the pixels values on each plane of a range of RA-DEC planes in a FITS cube.
*/
function summedPixelsSpectrumUnit(unit) {
  switch (unit) {
    case "Jy/beam":
      return "Jy";
      break;

    case "erg/s/cm^2/A/arcsec^2":
      return "erg/s/cm^2/A";
      break;

    default:
      return "";
  }
}


/*
** A function which returns the units of a 2D array resulting from
** the summation of a range of RA-DEC planes in a FITS cube.
*/
function summedPixelsUnit(unit) {
  switch (unit) {
    case "Jy/beam":
      return "Jy/beam * km/s";
      break;

    case "erg/s/cm^2/A/arcsec^2":
      return "erg/s/cm^2/arcsec^2";
      break;

    default:
      return "";
  }
}

/*
** A function which returns a factor
** for the display of physical quantity
** The returned values are based on experience rather
** than on a purely rational approach
*/
function unitRescale(unit) {
  switch (unit) {
    case "Jy/beam":
      return 1.0;
      break;

    case "erg/s/cm^2/A/arcsec^2":
//     return 1e18;
      return 1.;
      break;

    case "erg/s/cm^2/A":
//      return 1e12;
      return 1.;

    default:
      return 1.0;

  }
}

/*
** A function which sums the values of a array
** between two indices i0 (included ) and i1 ( excluded )
** and returns the sum multiplied by a coefficient coeff.
*/
function sumArr(arr, i0, i1, coeff) {
    console.log("SumArr");
    console.log(arr);
    console.log("i0 : "+i0 + " , i1 : " + i1 + " coeff :  "+ coeff);
    i0 = Math.max(0, i0);
    i1 = Math.min(arr.length-1, i1);

    if (i0 > i1) [i0, i1] = [i1, i0];
    return coeff * arr.slice(i0, i1).reduceRight(function(a, b) {return a + b;});
}

/*
 A function which parses a string into an array of floats. The string is expected
 to be a comma separated list of textual representation of decimal numbers.
 A range [min, max[ can be provided,  then the values are considered valif if and only if they lie in that range.

 Return an array of float numbers or None.
*/
function str2FloatArray(s, range=false) {
  x = s.split(",")
  result = undefined;

  y = x.map(function(z) {return parseFloat(z)});
  if (range) {
    w = y.map(function(t) { return ( !isNaN(t) &&  (range[0] <= t) && (t < range[1]) )});
  }
  else {
    w = y.map(function(t) { return true;});
  }

  if (w.reduce(function(accum, u){ return accum && u}, true)) {
    result = y;
  }

  return result;
}

/*
** A function which creates a document fragment out of an HTML string and appends it to the content of an existing element.
** The HTML string is assumed to describe a single element ( e.g. one signle div, p, etc. ).
** Returns the created element.
*/
function createAndAppendFromHTML(html, element) {
  var template = document.createElement('template');
  template.innerHTML = html.trim();
  $(element).append(template.content.cloneNode(true));
  return element.lastChild;
}

/*
** Two functions to log when a function is entered and exited
*/
function ENTER() {
  var caller = ENTER.caller;
  if (caller == null) {
    result = "_TOP_";
  }
  else {
    result = caller.name + ": entering";
  }
  console.log(result + ": entering");
}

function EXIT() {
  var caller = EXIT.caller;
  if (caller == null) {
    result = "_TOP_";
  }
  else {
    result = caller.name + ": exiting";
  }
  console.log(result + ": exiting");
}
