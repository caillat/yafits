/*
** A set of classes and function definitions utilized by the
** differents flavours of SAMP.
**
** Author : Y. A. BA
** Date : 18th Septemner 2019
*/

/*
** Convert VOTABLE to HTML Format
*/
var DisplayVotable = function(xml){
    var vDocEl = xml.documentElement;
    var vTableEl = vDocEl.getElementsByTagName("TABLE")[0];
    var vFieldEls = vTableEl.getElementsByTagName("FIELD");
    var vTrEls = vTableEl.getElementsByTagName("TR");
    var ncol = vFieldEls.length;
    var nrow = vTrEls.length;
    var ic;
    var ir;
    
    var tableHtml = '';
    tableHtml += '<table border="1"><thead><tr>';
    for(ic=0; ic< ncol; ic++){
        tableHtml += "<th> <strong> " +vFieldEls[ic].getAttribute("name") +"</strong></th>" ;
    }
    tableHtml += "</tr></thead><tbody>";
    var vTdEls;
    for (ir = 0; ir < nrow; ir++) {
        vTdEls = vTrEls[ir].getElementsByTagName("TD");
        tableHtml += "<tr>";
        for (ic = 0; ic < ncol; ic++) {
        tableHtml += "<td>"+getTextContent(vTdEls[ic])+"</td>";
        }
        tableHtml += "</tr>";
    }
    tableHtml += "</tbody></table>";
    return tableHtml;
};

var getTextContent = function(node) {
    var txt = "";
    var n;
    for (n = node.firstChild; n; n = n.nextSibling) {
        if (n.nodeType === 3 ||     // text
            n.nodeType === 4)    {  // CDATA section
            txt = txt + n.data;
        }
    }
    return txt;
};

/*Retrieve list of Ra/Dec from Votable*/
var GetRaDecCoord = function(xml){
    var vDocEl = xml.documentElement;
    var vTableEl = vDocEl.getElementsByTagName("TABLE")[0];
    var vFieldEls = vTableEl.getElementsByTagName("FIELD");
    var vTrEls = vTableEl.getElementsByTagName("TR");
    var ncol = vFieldEls.length;
    var nrow = vTrEls.length;
    var ic;
    var ir;
    
    
    var result = [];
    var numRa;
    var numDec;
    for(ic=0; ic< ncol; ic++){
        if(vFieldEls[ic].getAttribute("ucd") == "pos.eq.ra")
            numRa = ic;
        else if(vFieldEls[ic].getAttribute("ucd") == "pos.eq.dec")
            numDec = ic;
    }
    var vTdEls;
    for (ir = 0; ir < nrow; ir++) {
        vTdEls = vTrEls[ir].getElementsByTagName("TD");
        var radec = {};
        radec.RAInDD = getTextContent(vTdEls[numRa]);
        radec.DECInDD = getTextContent(vTdEls[numDec]);
        result.push(radec);
    }
    
    return result;
}
