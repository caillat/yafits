class Settings {
  constructor(viewer) {
    this.LUTSelectorId="LUTSelector"+"_"+viewer.getDivId();
    this.ITTSelectorId="ITTSelector"+"_"+viewer.getDivId();
    this.VideoModeSelectorId="VideoModeSelector"+"_"+viewer.getDivId();
    this.applyId="apply-settings"+"_"+viewer.getDivId();
    this.html = `
<div id="ModalSettingsForm" class="modal fade">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title">Settings</h1>
      </div>
      <div class="modal-body">
        <ul class="list-group list-group-horizontal">
          <li class="list-group-item ">
            <label for="LUTSelector">LUTs</label>
            <select name="LUTs" class="form-control" id="${this.LUTSelectorId}">
            </select>
          </li>

          <li class="list-group-item ">
            <label for="ITTSelector">ITTs</label>
            <select class="form-control" id="${this.ITTSelectorId}">
            </select>
          </li>

          <li class="list-group-item ">
            <label for="VideoModeSelector">Video mode</label>
            <select class="form-control" id="${this.VideoModeSelectorId}">
            </select>
          </li>

          <li class="list-group-item">
            <a class="form-control" id="${this.applyId}" class="btn btn-primary" href="#" role="button">Apply</a>
          </li>
        </ul>
      </div>
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- #ModalSettingsForm -->
`
    this.map = viewer.getMap();
    this.modal = createAndAppendFromHTML(this.html, $(this.map.getTargetElement()));
    this.button = document.createElement("button");
    this.button.setAttribute("type", "button");
    this.button.setAttribute("class", "btn btn-primary btn-sm");
    this.button.setAttribute("data-toggle", "modal");
    this.button.setAttribute("data-target", "#ModalSettingsForm");
    this.button.setAttribute("data-tooltip", "tooltip");
    this.button.setAttribute("title", "Set visualization parameters");
    let x = document.createElement("span");
    x.setAttribute("class", "fas fa-cog");
    this.button.appendChild(x);
  }

  setLUTSelectorIndex(index){
    $("#"+this.LUTSelectorId).prop('selectedIndex', index); 
  }

  get selectedLUT(){
    return $('#'+this.LUTSelectorId).find(':selected').text().trim();
  }

  setITTSelectorIndex(index) {
    $("#" + this.ITTSelectorId).prop('selectedIndex', index);
  }

  get selectedITT(){
    return $("#" + this.ITTSelectorId).find(':selected').text().trim();
  }

  setVideoModeSelectorIndex(index){
    $("#" + this.VideoModeSelectorId).prop('selectedIndex', index);   
  }

  get selectedVideoMode(){
    return $("#" + this.VideoModeSelectorId).find(':selected').text().trim();
  }

  get applyBtn() {
    return $("#"+this.applyId);
  }

  getButton() {
    return this.button;
  }

  appendLUT(name) {
    $('#' + this.LUTSelectorId).append(`<option>${name}</option>`);
  }

  appendITT(name) {
    $('#' + this.ITTSelectorId).append(`<option>${name}</option>`);
  }

  appendVM(name) {
    $('#' + this.VideoModeSelectorId).append(`<option>${name}</option>`);
  }
}; // End of class Settings