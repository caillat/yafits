class MarkersFactory extends ShapesFactory {
    constructor(viewer) {
        super(viewer, null);
        ShapesFactory.enter(this.constructor.name);
        this.source;
        this.layer;
        this.html=`
< !--Modal HTML Markup-- >
<div id="ModalMarkersForm" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
             <div class="modal-header">
                <h1 class="modal-title">Markers</h1>
            </div>
            <div class="modal-body">
                <form role="form" method="POST" action="">
                    <input type="hidden" name="_token" value="">
                    <div class="form-group">
                        <label class="control-label">Enter one set RA,DEC[,Id] per line in the text area.</label>
                        <div>
                            textarea class="form-control" rows="5" id="text_of_markers"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div>
                            <button type="button" class="btn btn-success" id="parse_text_of_markers">Check</button>
                        </div>
                    </div>

                    <div class="form-group">
                        <div>
                            <button type="button" class="btn btn-success" id="clear_markers">Clear</button>
                        </div>
                    </div>

                    <div class="form-group">
                        <div>
                            <button type="button" class="btn btn-success" id="refresh_markers_display">Refresh display</button>
                        </div>
                    </div>
                </form>
            </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->        
        `;
        this.modal = createAndAppendFromHTML(this.html, $(viewer.getMap().getTargetElement()));
        this.button = document.createElement("button");
        this.button.setAttribute("type", "button");
        this.button.setAttribute("class", "btn btn-primary btn-sm");
        this.button.setAttribute("data-toggle", "modal");
        this.button.setAttribute("data-target", "#ModalMarkersForm");
        this.button.setAttribute("data-tooltip", "tooltip");
        this.button.setAttribute("title", "Work with markers");
        this.button.append("M");

        this.draw = new ol.interaction.Draw({ source: this.source, type: 'Point' });
        this.numMarkers = 0;
        this.draw.on("drawend", event => {
            let coord = event.feature.getGeometry().getCoordinates();
            let withFlux = false;
            numMarkers += 1;
            event.feature.set('label', 'M_' + numMarkers);
            appendMarkerTA(_raLabelFormatter.format(coord[0]), _decLabelFormatter.format(coord[1]), 'm_' + _marker_index_1, coord[0], coord[1], null, false, _header);
        });

        this.markers = [];
        this.new_markers = [];
        ShapesFactory.exit();
    }

    getButton() {
        return this.button;
    }

    addMarkers(arrayOfMarkers) {
        ShapesFactory.enter(MarkersFactory.addMarkers.name);
        for (i = 0; i < arrayOfMarkers.length; i++) {
            var x = _raDDtoPixelConverter.convert(arrayOfMarkers[i]["RAInDD"]);
            var y = _decDDtoPixelConverter.convert(arrayOfMarkers[i]["DECInDD"]);
            console.log("x = " + x + ", y = " + y);
            this.numMarkers += 1;
            let pf = new ol.Feature({ geometry: new ol.geom.Point([x, y]) });;
            this.source.addFeature(pf);
            pf.set('label', arrayOfMarkers[i]["label"]);
        }
        this.source.refresh();
        ShapesFactory.exit()
    }

    getAllMarkers() {
        ShapesFactory.enter(MarkersFactory.getAllMarkers.name)
        var allMarkers = [];
        this.source.getFeatures().forEach(function (feature) {
            if (feature.getGeometry().getType() == 'Point') {
                this.allMarkers.push(feature);
            }
        });
        console.log("this.getAllMarkers : exiting");
        return allMarkers;
    }

    deleteMarkers(arrayOfFeatures) {
        ShapesFactory.enter(MarkersFactory.deleteMarkers.name);
        for (var i = 0; i < arrayOfFeatures.length; i++) {
            console.log("Deleting one marker");
            _marker_source_1.removeFeature(arrayOfFeatures[i]);
        }
        _marker_source_1.refresh();
        ShapesFactory.exit();
    }

    _parse(value, pat) {
        ShapesFactory.enter(MarkersFactory._parse.name);
        console.log("value = " + value);
        var result = value.match(pat);
        console.log("result b4 = " + result);
        if (result) console.log("input = " + result.input);
        if (result) result = result.map(function (elem, index) { if (index > 0) return Number(elem); else return elem; });
        console.log("result after = " + result);
        ShapesFactory.exit();
        return result;
    }

    RAtoString(RA) {
        return RA.hour + "h" + RA.minute + "m" + RA.second + "s";
    }

    RAtoDecimalDegree(RA) {
        return RA.hour * 15 + RA.minute / 4 + RA.second / 240;
    }

    checkRA(RAText) {
        let pat;
        let result;

        var RA = { hour: 0, minute: 0, second: 0, degree: 0, decdegree: 0 };
        value = RAText;
        pat = /(\d{1, 2})[hH:\s]\s*(\d{1, 2})[mM:\s]\s*(\d{1, 2}(\.\d+)?)[sS]?/;
        result = parse(value, pat);
        if (result &&
            (result[1] < 24) &&
            (result[2] < 60) &&
            (result[3] < 60)) {
            console.log(result);
            RA.hour = result[1];
            RA.minute = result[2];
            RA.second = result[3];
            RA.decdegree = RA.hour * 15 + RA.minute / 4 + RA.second / 240;
            return RA;
        }
        else {
            pat = /(\d{1,3})\.(\d+)/;
            result = parse(value, pat);
            if (result) {
                RA.decdegree = parseFloat(value);
                RA.hour = Math.floor(RA.decdegree / 15);
                RA.minute = Math.floor((RA.decdegree / 15 - RA.hour) * 60);
                RA.second = (RA.decdegree / 15 - RA.hour - RA.minute / 60) * 3600;
                RA.second = Math.floor(RA.second * 1000) / 1000.;
                return RA;
            }
            else {
                alert("Wrong input for RA. Valid symbols for hours : [hH: ], minutes : [mM: ] and seconds : [sS]");
                console.log("Wrong input for RA");
                return null;
            }
        }
    }

    DECtoString(DEC) {
        return (DEC.negative ? '-' : '') + DEC.degree + "\xB0" + DEC.arcminute + "'" + DEC.arcsecond + "\"";
    }

    DECtoDecimalDegree(DEC) {
        var result = DEC.degree + DEC.arcminute / 60. + DEC.arcsecond / 3600.
        if (DEC.negative) result = -result;
        return result;
    }

    checkDEC(DECText) {
        ShapesFactory.enter(MarkersFactory.checkDEC.name);
        var returned = null;
        var DEC = { negative: false, degree: 0, arcminute: 0, arcsecond: 0, decdegree: 0. };
        value = DECText.trim();
        var pat = /-?(\d{1, 2})[dD\xB0:\s]\s*(\d{1, 2})['mM:\s]\s*(\d{1, 2}(\.\d+)?)[s"]?/;
        var result = parse(value, pat);
        if (result &&
            ((result[1] == 90 && result[2] == 0 && result[3] == 0) ||
                (result[1] < 90 && result[2] < 60 && result[3] < 60))) {
            console.log("result[0] = " + result[0] + ", charCodeAt(0) == " + result[0].charCodeAt(0));
            if (result[0][0] == '-' || result[0].charCodeAt(0) == 8722 || result[0].charCodeAt(0) == 45) {
                DEC.negative = true;
            }
            DEC.degree = result[1];
            DEC.minute = result[2];
            DEC.second = result[3];
            DEC.decdegree = (DEC.degree + DEC.minute / 60. + DEC.second / 3600.) * (DEC.negative ? -1 : 1);
            console.log("DEC = " + JSON.stringify(DEC, 0, 4));
            returned = DEC;
        }
        else {
            if (value.charCodeAt(0) == 8722 || value.charCodeAt(0) == 45) { value = "-" + value.slice(1); }
            var pat1 = /-?(\d{1, 3}(\.\d+)?)/;
            var result1 = parse(value, pat1);
            if (result1) {
                DEC.decdegree = result1[1];
                var i0 = 0;
                if (value[0] == "-") {
                    i0 = 1;
                    DEC.decdegree = -DEC.decdegree;
                }
                DEC.negative = DEC.decdegree < 0.0;
                //
                DEC.degree = Math.abs(Math.floor(DEC.decdegree));
                DEC.minute = Math.floor((DEC.decdegree - DEC.degree) * 60);
                DEC.second = (DEC.decdegree - DEC.degree - DEC.minute / 60) * 3600;
                if (DEC.second < 0.) DEC.second = 0.; if (DEC.second > 60) DEC.second = 60.;
                returned = DEC;
            }
        }

        if (returned == null) {
            alert("Wrong input for DEC. Valid symbols for degrees : [\xB0dD: ], minutes : [mM': ] and seconds : [sS\"]");
            console.log("Wrong input for DEC");
        }
        ShapesFactory.exit();
        return returned;
    }

    checkRADEC(RAText, DECText) {
        return { 'RA': checkRA(), 'DEC': checkDEC() };
    }

    activate_markers() {

        $('#text_of_markers').bind('input propertychange', function () {
            $("#place_markers").attr('disabled', true);
        });

        $("#parse_text_of_markers").click(function () {
            this.new_markers = parse_text_of_markers();
        });

        $("#refresh_markers_display").click(function () {
            console.log('$("#refresh_markers_display").click(function() {: entering');
            this.deleteMarkers(this.getAllMarkers());
            console.log("About to draw these markers " + JSON.stringify(this.new_markers, 0, 4));
            this.addMarkers(this.new_markers);
            console.log('$("#refresh_markers_display").click(function() {: exiting');
        });

        $("#clear_markers").click(function () {
            $("#text_of_markers").val('');
            this.deleteMarkers(this.getAllMarkers());
        })

        $("#refresh_markers_display").attr('disabled', true);
    }

    enable_place_markers() {
        $("#refresh_markers_display").attr('disabled', false);
    }

    /*
    ** Parse the content of div "text_of_markers" row after row
    ** and populate the content of an array new_markers with
    ** the content of the parsing.
    */
    parse_text_of_markers() {
        ShapesFactory.enter(MarkersFactory.parse_text_of_markers.name);
        var markers = [];
        var content = $("#text_of_markers").val();
        var lines = content.split(/\r|\r\n|\n/);
        for (var i = 0; i < lines.length; i++) {
            let line = lines[i].trim();
            if (line.length > 0) {
                var fields = line.split(",");
                if (fields.length < 2) {
                    alert("Line '" + line + "' looks incomplete");
                    return (false);
                }
                var label = "" + i;
                if (fields.length >= 3) {
                    label = fields[2];
                }
                var RA = null;
                var DEC = null;
                if ((RA = checkRA(fields[0])) && (DEC = checkDEC(fields[1]))) {
                    markers.push({ "RAInDD": RA.decdegree, "DECInDD": DEC.decdegree, "id": (fields.length == 3) ? fields[2] : "", "label": label });
                }
                else {
                    return (false);
                }
            }
        }

        enable_place_markers();
        console.log(JSON.stringify(new_markers, 0, 4));
        ShapesFactory.exit();
        return markers;
    }

    /*
    ** Append data associated with a marker to the text_of_markers textarea.
    ** The data are RA , DEC and optionally an identifying string id.
    ** RA and DEC must be expressed in the forms hour:minute:second.second for 'RA' and degree:minute:second.second for DEC.
    ** E.g. RA = 20:34:24.178, DEC = +6:54:46.222
    */
    appendMarkerTA(RA, DEC, label, iRA, iDEC, id = null, withFlux = false, FITSHeader = null) {
        ta = $("#text_of_markers");
        ta.val(ta.val() + RA + ", " + DEC + (id ? id : "") + ", " + label + ", " + iRA + ", " + iDEC + "\n");
    }

    /*
    ** Clean the markers text area
    */
    cleanMarkerTA() {
        $("#text_of_markers").val("");
    }

    /*
    ** When we receive data via SAMP, check the data in the list box and draw them on the image.
    */
    checkAndDrawSampMarkers() {
        ShapesFactory.enter(MarkersFactory.checkAndDrawSampMarkers.name);
        new_markers = parse_text_of_markers();
        console.log('draw aladin ra/dec values {: entering');
        this.deleteMarkers(this.getAllMarkers());
        console.log("About to draw these markers " + JSON.stringify(new_markers, 0, 4));
        this.addMarkers(new_markers);
        ShapesFactory.exit();
    }
}


