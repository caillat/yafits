
class InfosBlock {
    static enter(what) {
        console.group(this.name + "." + what);
    }

    static exit() {
        console.groupEnd();
    }

    constructor(viewer, infosLine) {
        InfosBlock.enter(this.constructor.name);
        this.viewer = viewer;
        this.infosLine = infosLine;
        this.infosBlockId="ModalInfosBlock"+"_"+this.infosLine.id;
        this.infosBlockTitleId="ModalInfosBlockTitle"+"_"+this.infosLine.id;
        this.infosBlockBodyId = "ModalInfosBlockBody" + "_" + this.infosLine.id;
        this.html = `<div class="modal fade" id="${this.infosBlockId}" tabindex="-1" role="dialog" aria-labelledby="${this.infosBlockTitleId}" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="${this.infosBlockTitleId}">Infos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="${this.infosBlockBodyId}">
      </div>
      <textarea style="height:0px;width:0px;opacity:0">
      </textarea>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-sm fas fa-files-o" data-tooltip="tooltip" title="Copy verbatim"></button>
        <button type="button" class="btn btn-primary btn-sm">Copy as JSON</button>
        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>`;
        this.infosBlock = createAndAppendFromHTML(this.html, viewer.getMap().getTargetElement());
        this.infosBlockTitle = document.getElementById(this.infosBlockTitleId);
        this.infosBlockBody = document.getElementById(this.infosBlockBodyId);
        this.hiddenInfosBlock = this.infosBlock.querySelector("textarea");
        this.verbatimCopyBtn = this.infosBlock.querySelectorAll("button")[1]
        this.jsonCopyBtn = this.infosBlock.querySelectorAll("button")[2];

        this.jsonCopyBtn.onclick = this.jsonCopy.bind(this);
        this.verbatimCopyBtn.onclick = this.verbatimCopy.bind(this);
        InfosBlock.exit();
    }

    format(floatValue) {
        let result = floatValue;
        if (typeof result === "number" && !Number.isInteger(result)) {
            result = result.toExponential(4);
        }
        return result;
    };


    headline(title) {
        InfosBlock.enter(this.headline.name);
        console.log(this.infosBlockId);
        this.infosLine.innerHTML = title;
        this.infosLine.innerHTML += ` <button type="button" class="btn btn-outline-link btn-sm" data-toggle="modal" data-target="#${this.infosBlockId}">etc.</button>`;
        console.log("infosLine="+this.infosLine.innerHTML);
        InfosBlock.exit();
    }

    populate(title, collection) {
        InfosBlock.enter(this.populate.name);
        this.title = title;
        this.collection = collection;
        console.log(collection);
        this.infosBlockBody.innerHTML = "<b>" + title + " <br><br>";
        for (var k in collection) {
            this.infosBlockBody.innerHTML += k + ":" + this.format(collection[k]["value"]) + " " + collection[k]["unit"] + "<br>";
        }
        InfosBlock.exit()
    }

    verbatimCopy() {
        InfosBlock.enter(this.verbatimCopy.name);
        this.hiddenInfosBlock.value = this.title + "\r\n\r\n";
        this.hiddenInfosBlock.value += "file:" + this.viewer.getRelFITSFilePath() + "\r\n";
        if (this.viewer.isThreeD()) {
            this.hiddenInfosBlock.value += "slice index:" + this.viewer.getSliceIndex() + "\r\n";
        }
        for (var k in this.collection) {
            this.hiddenInfosBlock.value += k + ":" + this.format(this.collection[k]["value"]) + " " + this.collection[k]["unit"] + "\r\n";
        }
        this.hiddenInfosBlock.select();
        try {
            var success = document.execCommand('copy');
        }
        catch (err) {
            alert("Copy failed. " + err);
        }
        InfosBlock.exit();
    }

    jsonCopy() {
        InfosBlock.enter(this.jsonCopy.name);
        let context = {};
        context["file"] = this.viewer.getRelFITSFilePath();
        if (this.viewer.isThreeD()) {
            context["slice index"] = this.viewer.getSliceIndex();
        }
        this.hiddenInfosBlock.value = JSON.stringify($.extend(context, this.collection));
        this.hiddenInfosBlock.select();
        try {
            var success = document.execCommand('copy');
        }
        catch (err) {
            alert("Copy failed. " + err);
        }
        InfosBlock.exit();
    }
}
