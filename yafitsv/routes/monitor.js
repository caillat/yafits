var express = require('express');
var router = express.Router();
const request = require('request');

var yafitssHost = process.env.YAFITSS_HOST;
var yafitssPort = process.env.YAFITSS_PORT;

console.log("I'll collaborate with ..." + yafitssHost + ":" + yafitssPort + " ...for the FITS files services");

/*
** Prepare the communication with the FITS server ( yafitss )
*/
var clienthttp = {
  server : "http://"+yafitssHost+":"+yafitssPort+"/artemix",
  getDataBlockInfos : function(callback) {
    request.post(this.server+"/getDataBlockInfos", function(error, response, body){
      callback(error, response, body);
    });
  }
};

/*
** Define the http nodejs route
*/
router.get("/", function(req, res, next) {
  console.log("monitor router.get("/", function(req, res, next) { : entering" );
  clienthttp.getDataBlockInfos((error, response, body) =>  {
    console.log("getDataBlockInfos callback : entering");
    if (error ){
      var message = error.toString();
      res.setHeader('Content-type', 'application/json');
      res.send(JSON.stringify({"status":false, "message": message}));
    }
    else if (response["statusCode"] == 500) {
      res.send(JSON.stringify({status: false, message: response["body"]}));
    }
    else if (response["body"]["status"] == false) {
      res.render("error", {message: response["body"]["message"], error : {"status" : "", "stack" : ""}});
    }
    else {
      let x = JSON.parse(response["body"])
      console.log(JSON.parse(response["body"]));
      res.render("monitor", {when: x["result"]["when"], maxidle:x["result"]["maxidle"], hostname: x["result"]["hostname"], cpu: x["result"]["cpu"], memory: x["result"]["memory"], dataBlockInfos: x["result"]["dataBlocks"]});
    }
    console.log("getDataBlockInfos callback : exiting");
  });
  console.log("monitor router.get("/", function(req, res, next) { : exiting" );
});

module.exports = router;
