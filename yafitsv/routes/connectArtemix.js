var express = require('express');
var util = require('util');
var router = express.Router();

router.get('/',
  function (req, res, next) {
    var path_to_fits = req.query.relFITSFilePath;
    var params = {
      path_to_fits : path_to_fits
    };
    res.render('connectArtemix', params);
  }
);

module.exports = router;
