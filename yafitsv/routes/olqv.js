var express = require('express');
var util = require('util');
var fs = require('fs'), PNG = require('node-png').PNG;
var router = express.Router();
const path = require("path");
const uuidv1 = require('uuid/v1');
const request = require('request');
const url = require('url');
const obj2gltf = require('obj2gltf');
const dirs = require('../dirs')

var ENTER = function () { console.log(arguments.callee.name + ": entering."); };
var EXIT = function () { console.log(arguments.callee.name + ": exiting."); };

const PNG_ROOT_DIR = dirs.PNG_ROOT_DIR;
const OBJ_ROOT_DIR = dirs.OBJ_ROOT_DIR;
const SAMP_DIR = dirs.SAMP_DIR;

var yafitssHost = process.env.YAFITSS_HOST;
var yafitssPort = process.env.YAFITSS_PORT;

var yafitsvHost = process.env.YAFITSV_HOST;
var yafitsvPort = process.env.YAFITSV_PORT;

console.log("I'll collaborate with ..." + yafitssHost + ":" + yafitssPort + " ...for the FITS files services");

var useSAMP = true;
console.log("Using SAMP ..." + useSAMP);

var renderingCapabilities = null;

/***************************************************/
/* Define the interface with the HTTP FITS server. */
/***************************************************/

var clienthttp = {
  server: "http://" + yafitssHost + ":" + yafitssPort + "/artemix",

  setData: function (relFITSFilePath, sessionID, callback) {
    request(this.server + "/setData?relFITSFilePath=" + relFITSFilePath, { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  degToHMSDMS: function (relFITSFilePath, iRA, iDEC, sessionID = 0, callback) {
    request(this.server + "/degToHMSDMS?relFITSFilePath=" + relFITSFilePath + "&iRA=" + iRA + "&iDEC=" + iDEC, { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  rangeToHMS: function (relFITSFilePath, sessionID, iRA0, iRA1, iRAstep, callback) {
    request(this.server + "/rangeToHMS?relFITSFilePath=" + relFITSFilePath + "&iRA0=" + iRA0 + "&iRA1=" + iRA1 + "&iRAstep=" + iRAstep, { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  rangeToDMS: function (relFITSFilePath, sessionID, iDEC0, iDEC1, iDECstep, callback) {
    request(this.server + "/rangeToDMS?relFITSFilePath=" + relFITSFilePath + "&iDEC0=" + iDEC0 + "&iDEC1=" + iDEC1 + "&iDECstep=" + iDECstep, { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  getSlice: function (relFITSFilePath, sessionID, iFREQ, step, callback) {
    request(this.server + "/getSlice?relFITSFilePath=" + relFITSFilePath + "&iFREQ=" + iFREQ, { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  getAverage: function (relFITSFilePath, sessionID, step, iDEC0, iDEC1, iRA0, iRA1, iFREQ0, iFREQ1, retFITS, callback) {
    request(this.server + "/getAverage?relFITSFilePath=" + relFITSFilePath + "&step=" + step + "&iFREQ0=" + iFREQ0 + "&iFREQ1=" + iFREQ1 + "&iRA0=" + iRA0 + "&iRA1=" + iRA1 + "&startZ=" + startZ + "&endZ=" + endZ,
      { json: true }, function (error, response, body) {
        callback(error, response, body);
      });
  },

  getSpectrum: function (relFITSFilePath, sessionID, iRA, iDEC, iFREQ0, iFREQ1, callback) {
    request(this.server + "/getSpectrum?relFITSFilePath=" + relFITSFilePath + "&iRA=" + iRA + "&iDEC=" + iDEC + "&iFREQ0=" + iFREQ0 + "&iFREQ1=" + iFREQ1,
      { json: true }, function (error, response, body) {
        callback(error, response, body);
      });
  },

  createFits: function (relFITSFilePath, iRA, iDEC, callback) {
    let input = {
      "relFITSFilePath": relFITSFilePath,
      "iRA": iRA,
      "iDEC": iDEC
    };
    request.post(this.server + "/createFits", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  getAverageSpectrum: function (relFITSFilePath, sessionID, iDEC0, iDEC1, iRA0, iRA1, retFITS, callback) {
    request(this.server + "/getAverageSpectrum?relFITSFilePath=" + relFITSFilePath + "&iDEC0=" + iDEC0 + "&iDEC1=" + iDEC1 + "&iRA0=" + iRA0 + "&iRA1=" + iRA1 + "&retFITS=" + retFITS,
      { json: true }, function (error, response, body) {
        callback(error, response, body);
      });
  },

  getSumOverSliceRectArea: function (relFITSFilePath, sessionID, iFREQ, iRA0, iRA1, iDEC0, iDEC1, callback) {
    request(this.server + "/getSumOverSliceRectArea?relFITSFilePath=" + relFITSFilePath +
      "&sessionID=" + sessionID +
      "&iFREQ=" + iFREQ +
      "&iRA0=" + iRA0 +
      "&iRA1=" + iRA1 +
      "&iDEC0=" + iDEC0 +
      "&iDEC1=" + iDEC1,
      { json: true },
      function (error, response, body) {
        callback(error, response, body)
      });
  },

  RADECRangeInDegrees: function (relFITSFilePath, sessionID, callback) {
    request(this.server + "/RADECRangeInDegrees?relFITSFilePath=" + relFITSFilePath, { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  getHeader: function (relFITSFilePath, sessionID, callback) {
    request(this.server + "/getHeader?relFITSFilePath=" + relFITSFilePath, { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  getOneSliceAsPNG: function (relFITSFilePath, iFREQ, ittName, lutName, vmName, sessionID, callback) {
    let input = {
      "relFITSFilePath": relFITSFilePath,
      "iFREQ": iFREQ,
      "ittName": ittName,
      "lutName": lutName,
      "vmName": vmName,
      "sessionID": sessionID
    };
    request.post(this.server + "/getOneSliceAsPNG", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  getSummedSliceRangeAsPNG: function (relFITSFilePath, iFREQ0, iFREQ1, ittName, lutName, vmName, sessionID, callback) {
    let input = {
      "relFITSFilePath": relFITSFilePath,
      "iFREQ0": iFREQ0,
      "iFREQ1": iFREQ1,
      "ittName": ittName,
      "lutName": lutName,
      "vmName": vmName,
      "sessionID": sessionID
    };
    request.post(this.server + "/getSummedSliceRangeAsPNG", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  getContours: function (relFITSFilePath, optParams, callback) {
    let input = { "relFITSFilePath": relFITSFilePath, "optParams": optParams };
    request.post(this.server + "/getContours", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  getYtObj: function (relFITSFilePath, iRA0, iRA1, iDEC0, iDEC1, iFREQ0, iFREQ1, callback) {
    // retrieve the product name, i.e. the simple filename without extension nor prefix.
    var product = path.parse(relFITSFilePath).name;
    let input = {
      "relFITSFilePath": relFITSFilePath,
      "product": product,
      "iRA0": iRA0,
      "iRA1": iRA1,
      "iDEC0": iDEC0,
      "iDEC1": iDEC1,
      "iFREQ0": iFREQ0,
      "iFREQ1": iFREQ1
    };
    request.post(this.server + "/getYtObj", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  measureContour: function (relFITSFilePath, iFREQ, contour, level, callback) {
    let input = { "relFITSFilePath": relFITSFilePath, "iFREQ": iFREQ, "contour": contour, "level": level };
    request.post(this.server + "/measureContour", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  measureBox: function (relFITSFilePath, iFREQ, iRA0, iRA1, iDEC0, iDEC1, callback) {
    let input = {
      "relFITSFilePath": relFITSFilePath,
      "iFREQ": iFREQ,
      "iRA0": iRA0, "iRA1": iRA1,
      "iDEC0": iDEC0, "iDEC1": iDEC1
    };
    request.post(this.server + "/measureBox", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  renderingCapabilities: function (callback) {
    request(this.server + "/renderingCapabilities", { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  ping: function (callback) {
    request(this.server + "/ping", { json: true }, function (error, response, body) {
      callback(error, response, body);
    });

  },

  /*getYtObj : function(relFITSFilePath) {
    let input = {
      "relFITSFilePath": relFITSFilePath
    };
    request.post(this.server+"/getYtObj", {json: true, body:input}, function(error, response, body){
      callback(error, response, body);
    });
  }*/
}


// Utility function to determine  will be the actual root of the URLs
// The logic below WORKS ONLY for situations like :
// http://some_host/some_prefix/<rest of the URL> where "some_prefix" must be seen as a part of the host part when a redirection is done.
// http://some_host/<rest of the URL> in classical situation.

var URLRoot = function (anurl) {
  console.log("URLRoot: entering");
  let x = url.parse(anurl);
  let result = x.protocol + "//" + x.hostname;
  if (x.port) {
    result += ":" + x.port;
  }
  let pathname_a = x.pathname.split("/");
  if (pathname_a.length >= 3 && pathname_a[2].length > 0) {
    result += "/" + pathname_a[1];
  }
  console.log("URLRoot: exiting");
  return result;
};


/******************/
/*   GET routes   */
/******************/


var filterHttpResponse = function (res, error, response, body) {
  if (error) {
    var message = error.toString();
    res.send(JSON.stringify({ "status": false, "message": "Internal error : " + message + ". Maybe the server is simply not running", "result": null }));
  }
  else if (response["statusCode"] == 500) {
    res.send(JSON.stringify({ "status": false, "message": response["body"] }));
  }
  else if (body["status"] == false) {
    res.send(JSON.stringify({ "status": false, "message": body["message"] }));
  }
  return;
};

/* GET home page. */
router.get('/',

  /* Check that the FITS server is up and waiting*/
  function (req, res, next) {
    console.log("pinq http request callback: arming");
    clienthttp.ping((error, response, body) => {
      console.log("pinq http request callback: entering")
      filterHttpResponse(res, error, response, body);
      console.log("FITS server is up and waiting " + JSON.stringify(body, 0, 4));
      console.log("pinq http request callback: exiting")
      next();
    });
    console.log("pinq http request callback: armed");
  },

  /* Retrieve the rendering capabilities */
  function (req, res, next) {
    console.log("renderingCapabilities http request callback: arming");
    clienthttp.renderingCapabilities((error, response, body) => {
      console.log("renderingCapabilities http request callback: entering")
      filterHttpResponse(res, error, response, body);
      console.log("renderingCapabilities http request callback: exiting")
      renderingCapabilities = body["result"];
      next();
    });
    console.log("renderingCapabilities http request callback: armed");
  },

  function (req, res) {
    console.log("getHeader http request callback: arming");
    var relFITSFilePath = req.query.relFITSFilePath;
    // retrieve the product name, i.e. the simple filename without extension nor prefix.
    console.log(JSON.stringify(req.headers));
    var product = path.parse(req.query.relFITSFilePath).name;
    if (req.headers.referer) {
      clienthttp.getHeader(relFITSFilePath, req.query.sessionID ? req.query.sessionID : 0, (error, response, body) => {
        console.log("getHeader http request callback: entering")
        if (error) {
          var message = error.toString();
          res.send(JSON.stringify({ error: message }));
        }
        else if (body["status"] == false) {
          var params = {
            title: 'Loading ' + relFITSFilePath,
            relFITSFilePath: relFITSFilePath,
            useSAMP: useSAMP,
            urlRoot: URLRoot(req.headers.referer)
          }
          res.render('setData', params);
        }
        else {
          header = JSON.parse(body["result"]);
          useSAMP = useSAMP && (header["INSTRUME"] != "SITELLE");
          var params = {
            title: 'View of ' + relFITSFilePath,
            relFITSFilePath: relFITSFilePath,
            product: product,
            header: header,
            useSAMP: useSAMP,
            renderingCapabilities: renderingCapabilities,
            yafitsvPort: process.env.YAFITSV_PORT,
            urlRoot: URLRoot(req.headers.referer)
          };

          // Let's render the page depending on the dimensionality of the dataset.
          // Ok it's a regular 3D dataset
          if ((header["NAXIS"] == 3 && header["NAXIS3"] > 1) || (header["NAXIS"] == 4 && header["NAXIS3"] > 1 && header["NAXIS4"] == 1)) {
            res.render('olqv', params);
          }
          // Ok it's a 2D dataset
          else if ((header["NAXIS"] == 2 && header["NAXIS1"] > 1 && header["NAXIS2"] > 1) ||
            (header["NAXIS"] == 4 && header["NAXIS1"] > 1 && header["NAXIS2"] > 1 && header["NAXIS3"] == 1 && header["NAXIS4"] == 1)) {
            res.render('olqv_2-5d', params)
          }
          // Ok it's something that we don't know how to display
          else {
            let params = {
              title: 'No display',
              relFITSFilePath: relFITSFilePath,
              header: header
            }
            res.render('olqv_error', params);
          }
        }
        console.log("getHeader http request callback: exiting")
      });
    }
    else {
      console.log("No referrer found");
      // If there is not referer let the browser without an answer.
      // This is not very elegant, but this works.
      console.log("Redirecting to " + yafitsvHost + ":" + yafitsvPort + req.originalUrl);
      res.redirect(yafitsvHost+":"+yafitsvPort+req.originalUrl);
    }
    console.log("getHeader http request callback: armed");
  });


/* A route to retrieve a PNG file indexed by a slice index */
router.get('/:product/:iFREQ', function (req, res, next) {
  iFREQ = req.params['iFREQ'];
  product = req.params['product'];

  var files = fs.readdirSync(PNG_ROOT_DIR + product);
  let sliceNumber = files.length;

  fs.createReadStream(PNG_ROOT_DIR + product + '/' + iFREQ + '.png')
    .pipe(new PNG({
      filterType: -1
    }))
    .on('metadata', function () {
      res.render('index', { title: 'Express', op: 'slice', iFREQ: iFREQ, product: product, width: this.width, height: this.height, sliceNumber: sliceNumber });
    });

});


/******************/
/*   POST routes  */
/******************/

/* The index post route. It will trigger a behaviour based on the value of "method" parameter */
router.post('/', function (req, res, next) {
  console.log("router.post('/', function(req, res, next) { : entering");

  var method = req.body.method;
  var self = res;
  var header;

  if (false) {
    ;
  }


  /*
    if (method === "getHMSDMS") {
      var iRA = req.body.iRA;
      var iDEC = req.body.iDEC;
  
      clienthttp.degToHMSDMS(req.body.relFITSFilePath,  parseInt(iRA), parseInt(iDEC), req.sessionID ?req.sessionID : 0, (error, response, body)=>{
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ data: body }));
      });
    }
  
    if (method === "getRangeHMS") {
      var startR = req.body.start;
      var endR = req.body.end;
      var step = req.body.step;
  
      clienthttp.rangeToHMS( req.body.relFITSFilePath, req.sessionID ?req.sessionID : 0, startR, endR, step, (error, response, body)=>{
        console.log("rangeToHMS");
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ data: body }));
      });
    }
  
    else if (method === "getRangeDMS") {
      let startR = req.body.start;
      let endR = req.body.end;
      let step = req.body.step;
  
      clienthttp.rangeToDMS(req.body.relFITSFilePath, req.sessionID ?req.sessionID : 0, startR, endR, step, (error, response, body)=>{
        console.log("rangeToDMS callback : entering");
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ data: body }));
        console.log("rangeToDMS callback : exiting");
      } );
    }
  */
  else if (method == "RADECRangeInDegrees") {
    clienthttp.RADECRangeInDegrees(req.body.relFITSFilePath, req.body.sessionID ? req.body.sessionID : 0, (error, response, body) => {
      console.log("RADECRangeInDegrees callback : entering");
      self.setHeader('Content-type', 'application/json');
      let result = { data: body };
      //console.log ("About to return '" + JSON.stringify(result, 0, 4) + "'");
      self.send(JSON.stringify(result));
      console.log("RADECRangeInDegrees callback : exiting");
    });
  }

  else if (method === "getHeader") {
    var relFITSFilePath = req.body.name;

    clienthttp.setData(relFITSFilePath, req.sessionID ? req.sessionID : 0, (error, response, body) => {
      console.log("getHeader callback : entering");
      if (error) {
        var message = error.toString();
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ error: message }));
      } else {
        header = JSON.parse(body);
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ data: { displayLimits: displayLimits, header: header } }));
      }
      console.log("getHeader callback : exiting");
    });
  }

  else if (method === "getSlice") {
    var sliceB = req.body.slice ? req.body.slice : 0;
    step = parseInt(req.body.step) ? parseInt(req.body.step) : 1;
    clienthttp.getSlice(req.body.relFITSFilePath, req.sessionID ? req.sessionID : 0, sliceB, step, (error, response, body) => {
      console.log("getSlice callback : entering ");
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify({ data: body }));
      console.log("getSlice callback : exiting ");
    });
  }

  else if (method === "getAverage") {
    var zmin = parseInt(req.body.zmin) ? parseInt(req.body.zmin) : 0;
    var zmax = parseInt(req.body.zmax) ? parseInt(req.body.zmax) : null;
    step = parseInt(req.body.step) ? parseInt(req.body.step) : 1;
    //console.log("### average values :  "+ zmin + "   "+zmax+"   "+step);
    clienthttp.getAverage(req.body.relFITSFilePath, req.sessionID ? req.sessionID : 0, step, null, null, null, null, zmin, zmax, (error, response, body) => {
      console.log("getAverage callback : entering");
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify({ data: body }));
      console.log("getAverage callback : exiting");
    });
  }

  else if (method === "getSpectrum") {
    var iRA = req.body.iRA;
    var iDEC = req.body.iDEC

    clienthttp.getSpectrum(req.body.relFITSFilePath, req.sessionID ? req.sessionID : 0, iRA, iDEC, null, null, (error, response, body) => {
      console.log("getSpectrum callback : entering");
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify({ data: body }));
      console.log("getSpectrum callback : exiting");
    });
  }

  else if (method === "getAverageSpectrum") {
    console.log("About to execute the code for getAverageSpectrum");
    var iRA0 = req.body.iRA0;
    var iDEC0 = req.body.iDEC0;
    var iRA1 = req.body.iRA1;
    var iDEC1 = req.body.iDEC1;

    var retFITS = useSAMP;
    clienthttp.getAverageSpectrum(req.body.relFITSFilePath, req.sessionID ? req.sessionID : 0, iDEC0, iDEC1, iRA0, iRA1, retFITS, (error, response, body) => {
      console.log("getAverageSpectrum callback : entering");
      //console.log("error = " + JSON.stringify(error));
      if (error) {
        var message = error.toString();
        console.log(message);
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ "status": false, "message": message }));
      }
      else if (body["status"] == false) {
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ "status": false, "message": body["message"] }));
      }
      else {
        if (useSAMP) {
          //create temporary file in SAMP_DIR
          var tempFile = "averageSpectrum." + uuidv1() + ".fits";
          var path = SAMP_DIR + "/" + tempFile;
          //fs.writeFileSync(path,res,{encoding:'utf8',flag:'w'})
          fitsContent = body["result"]["averageSpectrumFits"];
          fs.writeFile(path, fitsContent, function (error, fitsContent) {
            if (error) {
              var message = error.toString();
              console.log(message);
              res.send(JSON.stringify({ "status": false, "message": message }));
            }
            else {
              console.log("sending the name of the tmp FITS file along with the averageSpectrum");
              res.send(JSON.stringify({
                "status": true,
                "message": "",
                "result": {
                  "absFITSFilePath": "/" + tempFile,
                  "averageSpectrum": body["result"]["averageSpectrum"]
                }
              }
              )
              );
            }
          });
        }
        else {
          console.log("sending only the averageSpectrum")
          res.send(JSON.stringify({ "status": true, "message": "", "result": { "absFITSFilePath": "", "averageSpectrum": body["result"]["averageSpectrum"] } }));
        }
      }
      console.log("getAverageSpectrum callback : exiting");
    });
  }

  else if (method === "getSumOverSliceRectArea") {
    clienthttp.getSumOverSliceRectArea(
      req.body.relFITSFilePath,
      req.body.sessionID ? req.body.sessionID : 0,
      req.body.iFREQ ? parseInt(req.body.iFREQ) : 0,
      req.body.iRA0 ? parseInt(req.body.iRA0) : 0,
      req.body.iRA1 ? parseInt(req.body.iRA1) : null,
      req.body.iDEC0 ? parseInt(req.body.iDEC0) : 0,
      req.body.iDEC1 ? parseInt(req.body.iDEC1) : null,
      (error, response, body) => {
        console.log("getSumOverSliceRectArea callback : entering");
        if (error) {
          var message = error.toString();
          console.log(message);
          res.send(JSON.stringify({ "status": false, "message": message }));
        }
        else if (response["statusCode"] == 500) {
          self.send(JSON.stringify({ "status": false, "message": response["body"] }));
        }
        else if (body["status"] == false) {
          self.send(JSON.stringify({ "status": false, "message": body["message"] }));
        }
        else {
          self.send(JSON.stringify({ "status": true, "message": "", "result": body["result"] }))
        }
        console.log("getSumOverSliceRectArea callback : entering");
      }
    );
  }
  else if (method === "createFits") {
    console.log("createFits callback : entering");

    var iRA = req.body.iRA;
    var iDEC = req.body.iDEC;
    var relFITSFilePath = req.body.relFITSFilePath
    clienthttp.createFits(relFITSFilePath, iRA, iDEC, (error, response, body) => {
      //console.log("error = " + JSON.stringify(error));
      //console.log("response = " + JSON.stringify(response));
      //console.log("body = " + JSON.stringify(body));
      if (error) {
        var message = error.toString();
        console.log(message);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({ "status": false, "message": message }));
      }
      else if (body["status"] == false) {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({ "status": false, "message": body["message"] }));
      }
      else {
        console.log("createFits spectrum: with parameters (" + iRA + "," + iDEC + ")");
        //create temporary file in SAMP_DIR
        var tempFile = "spectrum." + uuidv1() + ".fits";
        var path = SAMP_DIR + "/" + tempFile;
        //fs.writeFileSync(path,res,{encoding:'utf8',flag:'w'})
        fitsContent = body["result"];
        fs.writeFile(path, fitsContent, function (error, fitsContent) {
          if (error) {
            var message = error.toString();
            console.log(message);
            res.send(JSON.stringify({ "status": false, "message": message }));
          }
          else {
            console.log("Successfully Written to File.");
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.send(JSON.stringify({ "status": true, "message": "", "result": "/" + tempFile }));
          }
        });
      }
      console.log("createFits callback : exiting");
    });
  }

  else if (method === "getObjects") {
    // Peform a distinct query against the a field Header.OBJECT . But don't forget to reject the null Header.OBJECT !
    Fitsinfo.distinct('Header.OBJECT', { 'Header.OBJECT': { $ne: null } }, function (err, objects) {
      if (err) {
        console.log(err);
      } else {
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({
          data: objects.sort(function (a, b) {
            var aA = a.toUpperCase();
            var bB = b.toUpperCase();
            return (aA < bB) ? -1 : (aA > bB) ? 1 : 0;
          })
        }));
      }
    });
  }

  else if (method === "getFiles") {
    var object = req.body.object;

    // Peform a distinct query against the a field Path with field Header.OBJECT == object
    Fitsinfo.distinct('Path', { "Header.OBJECT": object }, function (err, files) {
      if (err) {
        console.log(err);
      } else {
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ data: files }));
      }
    });
  }

  else if (method === "getFitsHeader") {
    var relFITSFilePath = req.body.relFITSFilePath;

    // Find the corresponding entry in DB
    //{fields: {"_id": 0}
    Fitsinfo.find({ "Path": relFITSFilePath }, { "_id": 0 }, function (err, fitsHeader) {
      if (err) {
        console.log(err);
      } else {
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ data: fitsHeader[0] }));
      }
    });
  }


  else if (method === "getHistory") {
    self.setHeader('Content-Type', 'application/json');
    self.send(JSON.stringify({ data: req.user.recentFiles }));
  }
  else {
    console.log("Do not know what to do here with " + JSON.stringify(req.body, null, 4));
  }

  console.log("router.post('/', function(req, res, next) { : exiting");

});

/*
** 1) Triggers a request to the FITS server for the header of a FITS file
** 2) Redirects the browser to the visualization ( visit ) page of the given FITS file.
*/
router.post('/setData', function (req, res, next) {
  console.log("router.post('/setData', function(req, res, next) { : entering");
  // we need a FITS header
  clienthttp.setData(encodeURI(req.body.relFITSFilePath), req.sessionID ? req.sessionID : 0, (error, response, body) => {
    console.log("setData callback : entering");
    if (error) {
      var message = error.toString();
      console.log(message);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify({ "status": false, "message": message }));
    }
    else if (body["status"] == false) {
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify({ "status": false, "message": body["message"] }));
    }
    // Everything is OK
    else {
      console.log("in setData referer = " + util.inspect(req.headers.referer));
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.redirect(req.headers.referer);
    }
    console.log("setData callback : exiting");
  });
  console.log("router.post('/setData', function(req, res, next) { : exiting");
});

router.post('/png', function (req, res, next) {
  console.log("router.post('/png', function(req, res, next) { : entering");

  clienthttp.getOneSliceAsPNG(req.body.relFITSFilePath, req.body.si, req.body.ittName, req.body.lutName, req.body.vmName, req.body.sessionID ? req.body.sessionID : 0, (error, response, body) => {
    console.log("getOneSliceAsPNG callback : entering");
    if (error) {
      console.log(error);
    }
    else {
      res.send(body);
    }
    console.log("getOneSliceAsPNG callback : exiting");
  });

  console.log("router.post('/png', function(req, res, next) { : exiting");
});

router.post('/sumpng', function (req, res, next) {
  console.log("router.post('/sumpng', function(req, res, next) {: entering");

  clienthttp.getSummedSliceRangeAsPNG(req.body.relFITSFilePath, req.body.si0, req.body.si1, req.body.ittName, req.body.lutName, req.body.vmName, req.body.sessionID ? req.body.sessionID : 0, (error, response, body) => {
    console.log("getSummedSliceRangeAsPNG callback entering");
    if (error) {
      console.log(error);
    }
    else {
      //result["path_to_png"] = result["path_to_png"].toString(); // ??? why do I get an object instead of a string . I have no idea; this does not happen with post('/png') !!!
      res.send(body);
    }
    console.log("getSummedSliceRangeAsPNG callback : exiting");
  });
  console.log("router.post('/sumpng', function(req, res, next) {: exiting");
});

router.post('/getContours', function (req, res, next) {
  console.log("router.post('/getContours', function(req, res, next) {: entering");
  clienthttp.getContours(req.body.relFITSFilePath, req.body.optParams, (error, response, body) => {
    console.log("getContours callback entering");
    if (error) {
      console.log(error);
    }
    else {
      res.send(body);
    }
    console.log("getContours callback : exiting");
  });
  console.log("router.post('/getContours', function(req, res, next) {: exiting");
});

router.post('/measureContour', function (req, res, next) {
  console.log("router.post('/measureContour', function(req, res, next){: entering");
  clienthttp.measureContour(req.body.relFITSFilePath, req.body.iFREQ, req.body.contour, req.body.level, (error, response, body) => {
    console.log("measureContour callback entering");
    if (error) {
      console.log(error);
    }
    else {
      res.send(body);
    }
    console.log("measureContour callback exiting");
  });
  console.log("router.post('/measureContour', function(req, res, next){: exiting");
});

router.post('/measureBox', function (req, res, next) {
  console.log("router.post('/measureBox', function(req, res, next){: entering");
  clienthttp.measureBox(req.body.relFITSFilePath, req.body.iFREQ, req.body.iRA0, req.body.iRA1, req.body.iDEC0, req.body.iDEC1, (error, response, body) => {
    console.log("measureBox callback entering");
    if (error) {
      console.log(error);
    }
    else {
      console.log(body);
      res.send(body);
    }
    console.log("measureBox callback exiting");
  });
  console.log("router.post('/measureBox', function(req, res, next){: exiting");
});

router.get("/getYtObj", function (req, res, next) {
  console.log('router.get("/", function(req, res, next) { : entering ');
  clienthttp.getYtObj(req.query.relFITSFilePath, req.query.iRA0, req.query.iRA1, req.query.iDEC0,
    req.query.iDEC1, req.query.iFREQ0, req.query.iFREQ1, (error, response, body) => {
      console.log("getYtObj callback : entering");
      if (error) {
        console.log(error);
      }
      else {
        console.log("body result " + body["result"]);
        var gltfFile = OBJ_ROOT_DIR + "/" + body["result"] + ".gltf";

        //create gltfFile
        obj2gltf(OBJ_ROOT_DIR + "/" + body["result"] + ".obj").then(function (gltf) {
          const data = Buffer.from(JSON.stringify(gltf));
          fs.writeFileSync(gltfFile, data);
        }).then(function () {
          console.log(gltfFile);
          res.render("getYtObj", { gltfFile: URLRoot(req.headers.referer) + "/" + body["result"] + ".gltf" });
        });

      }
      console.log("getYtObj callback : exiting");
    });
});

module.exports = router;
