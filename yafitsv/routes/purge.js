var express = require('express');
var router = express.Router();
const request = require('request');

var yafitssHost = process.env.YAFITSS_HOST;
var yafitssPort = process.env.YAFITSS_PORT;

console.log("I'll collaborate with ..." + yafitssHost + ":" + yafitssPort + " ...for the FITS files services");

var clienthttp = {
  server : "http://"+yafitssHost+":"+yafitssPort+"/artemix",
  purgeDataBlocks : function(callback) {
    request.post(this.server+"/purgeDataBlocks", function(error, response, body){
      callback(error, response, body);
    });
  }

};

router.get("/", function(req, res, next) {
  console.log("purge router.get("/", function(req, res, next) { : entering" );
  clienthttp.purgeDataBlocks((error, response, body) =>  {
    console.log("purgeDataBlocks callback : entering");
    if (error ){
      var message = error.toString();
      res.setHeader('Content-type', 'application/json');
      res.send(JSON.stringify({"status":false, "message": message}));
    }
    else if (response["statusCode"] == 500) {
      res.send(JSON.stringify({status: false, message: response["body"]}));
    }
    else if (response["body"]["status"] == false) {
      res.render("error", {message: response["body"]["message"], error : {"status" : "", "stack" : ""}});
    }
    else {
      let x = JSON.parse(response["body"])
      console.log(JSON.parse(response["body"]));
      res.redirect("/monitor");
    }
    console.log("purgeDataBlocks callback : exiting");
  });
  console.log("monitor router.get("/", function(req, res, next) { : exiting" );
});


module.exports = router;
