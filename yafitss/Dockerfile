#
# Adapted from the tutorial found at
# http://www.science.smith.edu/dftwiki/index.php/Tutorial:_Docker_Anaconda_Python_--_4
#
# Michel Caillat - 6 Mai 2019
#

#------------------------------------------------------------#
#                                                            #
#                  General required stuff                    #
#                                                            #
#------------------------------------------------------------#

# We will use Centos for our image
FROM centos:latest

# Updating Ubuntu packages
RUN yum install -y emacs

# Adding wget and bzip2
RUN yum install -y wget bzip2

# Install tcl stuff
RUN yum install -y tcl
RUN yum install -y tcl-devel

# Add sudo
RUN yum install -y sudo
RUN groupadd sudo
RUN cat /etc/group

# Installing nodejs
RUN yum -y install curl
RUN curl --silent --location https://rpm.nodesource.com/setup_9.x |sudo bash -
RUN yum -y install nodejs

#Install apidoc
RUN npm install -g apidoc

# Add user partemix with no password, add to sudo group
RUN adduser -G sudo -d /home/partemix  partemix
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
RUN ls /home/partemix

# Player is partemix
USER partemix
WORKDIR /home/partemix/
RUN chmod a+rwx /home/partemix/

# Anaconda installing
RUN wget https://repo.continuum.io/archive/Anaconda3-2019.03-Linux-x86_64.sh
RUN bash Anaconda3-2019.03-Linux-x86_64.sh -b
RUN rm Anaconda3-2019.03-Linux-x86_64.sh

# Set path to conda
ENV PATH /home/partemix/anaconda3/bin:$PATH

RUN pwd
RUN echo "PATH=$PATH"
#RUN find /home/partemix/anaconda3 -name "conda"
# Updating Anaconda packages
# RUN conda update conda
# RUN conda update anaconda
# RUN conda update --all

#------------------------------------------------------------#
#                                                            #
#               partemix related stuff                       #
#                                                            #
#------------------------------------------------------------#

# Create some directories expected by the application.
ARG YAFITS_FITSDIR
ARG YAFITS_FITSDIR2

ENV YAFITS_FITSDIR=$YAFITS_FITSDIR
ENV YAFITS_FITSDIR2=$YAFITS_FITSDIR2

# The root directory of the FITS files location (readonly). It'll have to be bound to an host directory.
RUN printenv
RUN sudo mkdir -p ${YAFITS_FITSDIR}
RUN sudo chown partemix:partemix ${YAFITS_FITSDIR}

# The first extension to the root directory of FITS files location (readonly). It'll have to be bound to an host directory.
RUN sudo mkdir -p ${YAFITS_FITSDIR2}
RUN sudo chown partemix:partemix ${YAFITS_FITSDIR2}

# The root directory of the PNG files (read/write). It'll have to be bound to an host directory.
RUN mkdir -p /home/partemix/dataroot/PNG

# The root directory of the yt Obj files (read/write). It'll have to be bound to an host directory.
RUN mkdir -p /home/partemix/dataroot/OBJ

# The root directory of the ApiDoc files (read/write).
RUN mkdir -p /home/partemix/dataroot/apidoc

# The directory for log files (read/write). It'll have to be bound to an host directory.
RUN mkdir /home/partemix/log


#------------------------------------------------------------#
#                                                            #
#                yafitss related stuff                       #
#                                                            #
#------------------------------------------------------------#

# The directory where the python FITS files server will reside.
RUN mkdir -p /home/partemix/yafitss

# Put the application files and module requirements in place.
COPY ./serverWsgi.py /home/partemix/yafitss/
COPY ./dataManager_michel*.py /home/partemix/yafitss/
COPY ./DataBlock.py /home/partemix/yafitss/
COPY ./requirements.txt /home/partemix/yafitss/
COPY ./apidoc.json /home/partemix/yafitss/

# python modules required by yafitss have to be installed
RUN pip install -r /home/partemix/yafitss/requirements.txt
RUN pip install yt
RUN pip install spectral-cube

# Generate the bottle server documentation
RUN ls -l /home/partemix/anaconda3/pkgs/matplotlib-3.0.3-py37h5429711_0/lib/
# RUN apidoc -f /home/partemix/yafitss/serverWsgi.py -c /home/partemix/yafitss/ -o /home/partemix/dataroot/apidoc

# ./yafitss/serverWsgi.michel will be listening at 4251
EXPOSE 4251

