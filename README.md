# yafits

Yet Another FITS viewer
This project allows to browse remotely in a WEB navigator a collection of FITS files and visualize their content.
Inspired from tools like ds9 or goview ( part of GILDAS software), it adds the "remote" dimension. 

## Docker's choice
After an initial phase where the project was built as a set of applications running on a (possibly virtual ) machine in a classical way, it's been decided to package 
those applications in a Docker container mostly to ease the distribution and the installation of the product. 

## Architecture
The facility is architectured as a composition of two Docker images :
* `yafitsv` : an HTTP server (NodeJS) that reacts to the user's requests such that navigating in the FITS collection or visually exploring the content of one selected FITS file.
* `yafitss` : an HTTP server (Python+Bottle) which actually performs all the hard work with FITS files ( browsing, loading in memory, extract parts to display, etc...) and is callable via a serie of REST APIs

The relations between the user's WEB browser and the containerized servers `yafitss` and `yafitsv` can be depicted as follows :

<pre>
                                                    YAFITS containers composition

+--------------------+                        +--------------------------------------------+
|                    |                        |                                            |
|                    |        Internet        |        +---------+         +---------+     |
|User's WEB navigator|&lt;----------------------&gt;|&lt;-----&gt; | yafitsv | &lt;-----&gt; | yafitss |     |
|                    |                        |        +---------+         +---------+     |
|                    |                        |                                            |
+--------------------+                        +--------------------------------------------+

</pre>

## Requirements

Have the applications `docker` (https://docs.docker.com/install/) and `docker-compose` (https://docs.docker.com/compose/install/) available on the host where you plan to deploy the project.

If your operating system is Linux please check that your docker's installation has been done completely and in particular that the post-installation steps have not been neglected. It's mandatory if you want to 
**manage Docker as a non-root user**, see https://docs.docker.com/install/linux/linux-postinstall/ 

Even if it's a not a requirement, having the Docker management and monitoring tool `portainer` (https://www.portainer.io/) installed is indiscutably a bonus.

## Getting the source code

Clone the project.

```
cd <..>
git clone https://gitlab.obspm.fr/caillat/yafits.git
```

## Before building the images composition

The steps below are absolutely mandatory for a successful build and execution of the images. **Do not skip them**.

### Adapt the configuration file

`yafits` is designed to be easily shared and deployed on a platform where the docker and docker-compose applications are available after having defined a couple of environment variables.
Prior to any other action :


*  Set the directory's project as the current directory :

`cd <..>/yafits`

* Copy the configuration file template `yafits.bashrc.dist` to a location and a name of your choice (e.g. `$HOME/.yafits.bashrc`) and edit its content accordingly with your local environment. 
* Execute the edited configuration file (e.g. `. $HOME/.yafits.bashrc`)

> When you perform an upate ( a git pull ) do not forget to check that your local configuration file `.yafits.bashrc` needs to be udpated given that the distributed file `yafits.bashrc.dist` may have changed.

### Customize the Welcome message file

Copy the file `<..>/yafits/yafitsv/public/Welcome.html.dist` to `<..>/yafits/yafitsv/public/Welcome.html`and possibly adapt its content to your specific service.

## Building the images composition
* Launch the build command : it'll take some time due to the CENTOS base and Anaconda installations.
 
`docker-compose build`


## Running the images compositions in containers

`
docker-compose up -d
`

Two containers should be running ready to process your requests at `http://${YAFITSV_HOST}:${YAFITSV:PORT}/browse`
( the two env variables are the ones you have defined in the configuration script. You will very likely use their value instead of their 
names as above. E.g. `http://juliette.obspm.fr:3022/` )

## Testing yafits

Open your browser at `http://${YAFITSV_HOST}:${YAFITSV:PORT}/browse`


## Technical details
The informations below are given for the curious of technical details. They may be useful in case of problems, but normally reading this 
section is not required.

### Account considerations
Instead of leaving the default account (root, uid==0) running the code, one uses a specific account created during the image build. Its name is `partemix` with uid==1000, gid==1001.

### Data files organization
Data files are on the one hand the FITS files (readonly) and on the other hand the PNG files (read/write). The software expects to find these files respectively in a `FITS`
directory and in a `PNG` directory both subdirectories of a same toplevel directory. The table below depicts the situation of the example of the `docker run`command ( see above)
where the host directory `/datartemix/datartemix` with its two subdirectories `FITS` and `PNG` is mounted on the container's directory `/home/partemix/dataroot` .

Exemple :

|host|container|
|:----|:----|
| /datartemix/ALMA |/home/partemix/dataroot  |
| /datartemix/ALMA/FITS , /tmp/PNG | /home/partemix/FITS , /home/partemix/PNG |


### Files considerations.
A characteristic of the container is that **all** the data that it accesses to are residing on the host filesystem. The container has NO data on his own; in other words all the data are persistent. A particular attention must therefore be paid to the protections defined on the data and the permissions to be set up for the applications executed by the container have the necessary access to them.

The informations given below are pertinent with the assumption that the image has been built with the Dockerfile left unchanged.

#### FITS files.
The FITS files which are the _raw material_ of yafits are accessed to as a readonly resource of the host filesystem. The most permissive way to ensure their readability
by the container is to make them readable by anyone (`chmod a+r`). A less permissive approach is to allow them to be read by the group gid==1001 and to put their owner into that group, possibly at the cost of creating such a group. The most restrictive access would be to have a user with uid==1000 ( which is not always possible ) and the FITS files belonging to that user.

#### PNG files.
The PNG files are temporary files generated on the fly for the visualization of FITS files contents. Consequently the directory used as a repository for these files must be writable by (uid==1000,gid==1001). As for the FITS files one can
choose between the three options 1) writable by anyone 2) writable by group==1001 with the PNG directory's owner in that group and 3) the PNG directory belonging to uid==1000 and writable by the owner. 

#### LOG files.
Both servers `yafitsv` and `yafitss` generates log files in a directory of the host which must be writable by uid=1000,gid=1001. Again one can use one of the strategies presented above for the PNG and FITS files.